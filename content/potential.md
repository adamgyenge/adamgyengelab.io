I am accepting MSc and Phd students, who are interested in algebraic geometry or representation theory. The [general advices](http://math.stanford.edu/~vakil/potentialstudents.html) from Ravi Vakil can be beneficial for every young person. 

If you are interested in doing a postdoc in Budapest, I can help you with looking up funding opportunities. For example, the [Rényi Institute](https://www.renyi.hu) has a well established postdoc programme.

