---
title: Publications
---

## Preprints

1. The Heisenberg algebra of a vector space (with Timothy Logvinenko)  
1. Fixed point counts and motivic invariants of bow varieties of affine type A (with Richárd Rimányi)  
[ [preprint](https://arxiv.org/abs/2409.03859 ) ]
1. Noncommutative projective partial resolutions and quiver varieties (with Søren Gammelgaard)  
[ [preprint](https://arxiv.org/abs/2406.00709) ]
1. Slicing up multigraded linear series  
[ [preprint](https://arxiv.org/abs/2306.12750) ]
1. Blow-ups and the quantum spectrum of surfaces (with Szilárd Szabó)  
 [ [preprint](https://arxiv.org/abs/2210.08939) ]
1. The Heisenberg category of a category (with Clemens Koppensteiner and Timothy Logvinenko)  
    [ [preprint](https://arxiv.org/abs/2105.13334) ]



## Published Articles

7. Euler characteristics of affine ADE Nakajima quiver varieties via collapsing fibers (with Lukas Bertsch and Balázs Szendrői)  
To appear in the _Journal of the London Mathematical Society_  
[ [preprint](https://arxiv.org/abs/2310.14361) ]
1. On a sequence of Grothendieck groups    
To appear in _Homology, Homotopy and Applications_
[ [preprint](https://arxiv.org/abs/2209.03209) ]
1. [A power structure over the Grothendieck ring of geometric dg categories](https://www.cambridge.org/core/services/aop-cambridge-core/content/view/055C4C173DDA7EA94A7AB85729192669/S0013091524000841a.pdf/a-power-structure-over-the-grothendieck-ring-of-geometric-dg-categories.pdf)  
_Proceedings of the Edinburgh Mathematical Society_ (2025)
1. [Kleinian singularities: some geometry, combinatorics and representation theory](https://link.springer.com/content/pdf/10.1365/s13291-024-00291-5.pdf) (with Lukas Bertsch and Balázs Szendrői)  
_Jahresbericht der Deutschen Mathematiker Vereinigung_ 126 (4) pp. 213–247 (2024)
1. [G-fixed Hilbert schemes on K3 surfaces, modular forms, and eta products](https://epiga.episciences.org/9189/pdf) (with Jim Bryan)  
_Épijournal de Géométrie Algébrique_, Volume 6 (2022), Article Nr. 6
1. [Rigid ideal sheaves and modular forms](https://academic.oup.com/qjmath/article-pdf/73/3/1035/45800031/haab066.pdf)  
_The Quarterly Journal of Mathematics_, Volume 73 (2022), Issue 3,  pp. 1035–1053
1. [Quot schemes for Kleinian orbifolds](https://www.emis.de/journals/SIGMA/2021/099/sigma21-099.pdf) (with Alastair Craw, Søren Gammelgaard and Balázs Szendrői)  
    _Special Issue on Enumerative and Gauge-Theoretic Invariants in honor of Lothar Göttsche, Symmetry, Integrability and Geometry: Methods and Applications (SIGMA)_ 17 (2021), 099
1. [Punctual Hilbert schemes for Kleinian singularities as quiver varieties](http://content.algebraicgeometry.nl/2021-6/2021-6-021.pdf) (with Alastair Craw, Søren Gammelgaard and Balázs Szendrői)  
    _Algebraic Geometry_ 8 (6) (2021) pp. 680--704
1. [Young walls and equivariant Hilbert schemes of points in type D](https://link.springer.com/content/pdf/10.1007%2F978-3-030-61958-9_3.pdf)  
    _Singularities and Their Interaction with Geometry and Low Dimensional Topology_, Birkhäuser Basel, 2021
1. [Canonical spectral coordinates for the Calogero-Moser space associated with the cyclic quiver](jnmp.pdf) (with Tamás Görbe)  
    _Journal of Nonlinear Mathematical Physics_ 27(2) (2020)
1. [The transition function of G2 over S6](https://www.emis.de/journals/SIGMA/2019/078/sigma19-078.pdf)  
   _Symmetry, Integrability and Geometry: Methods and Applications (SIGMA)_ 15 (2019), 078
1. [Euler characteristics of Hilbert schemes of points on simple surface singularities](https://link.springer.com/content/pdf/10.1007/s40879-018-0222-4.pdf)  (with Balázs Szendrői and András Némethi)  
    _European Journal of Mathematics_ 4 (2018), pp. 439-524
1. [Enumeration of diagonally colored Young diagrams](https://link.springer.com/content/pdf/10.1007%2Fs00605-016-0957-2.pdf)  
    _Monatshefte für Mathematik_ 183(1) (2017), pp. 143–157
1. [Euler characteristics of Hilbert schemes of points on surfaces with simple singularities](rnw139.pdf) (with Balázs Szendrői and András Némethi)  
    _International Mathematics Research Notices_ 2017.13 (2017) pp. 4152–4159.
1. [Hilbert scheme of points on cyclic quotient singularities of type (p,1)](https://link.springer.com/content/pdf/10.1007%2Fs10998-016-0146-z.pdf)  
    _Periodica Mathematica Hungarica_ Volume 73 (2016), Issue 1, pp. 93-99
1. [An efficient block model for clustering sparse graphs](mlg2010idbm.pdf) (with Janne Sinkkonen and András A. Benczúr)  
    _MLG 2010 - 8th International Workshop on Mining and Learning with Graphs_ (in conjunction with KDD 2010), 2010.
1. [A block model suitable for sparse graphs](mlg09sibm.pdf) (with Juuso Parkkinen, Janne Sinkkonen and Samuel Kaski)  
    _MLG 2009 - 7th International Workshop on Mining and Learning with Graphs_, 2009.
1. [Statistical methods for the investigation of scale-free networks](tdk.pdf) (in Hungarian), _Orsz. Tud. Diák Konf._, Vol. 29, 2009


## Other Writings

25. [Hilbert schemes of points on some classes of surface singularities](thesis_phd.pdf), PhD thesis (mathematics), 2016.
