---
title: Algebraic Topology and Homological Algebra
---

# Algebraic Topology and Homological Algebra

## Course info

- Class location and hours: Tuesday 14.30-16 (room H306) & Thursday 14.30-16 (room H405A). 
- Location: BME campus, building H
- Office hours: Before or after the classes (13.30-14.15 and 16-16.30), or ask in email. 

### Schedule
- Lectures start on 11/02 Tue
- 18/04-25/04 is Easter Break at BME 
- We will finish by 08/05
- There will be some make-up classes

## For BME students

Neptun code: BMETEAGMsMATHA-00  
Credits: 5  
Final grade: Homeworks + 2 short midterms

## For BSM students

Course title: Algebraic Topology and Homological Algebra --- ATH  
Final grade: Homeworks + 2 short midterms

__Extra classes__ (tentatively): occasionally Monday 8-10 at BSM site. BME students are also welcome.
- 03/03: exercise solving
- 24/03: Knot theory
- two more in April
- ...

## Course description

The goal of the course is to provide an introduction to the basic notions of homology and cohomology theory, and show some simple (and some more sophisticated) applications of these techniques in topology and algebra. Ideas from homology are present in all modern directions of mathematics, and we will show several appearances of those as well. 

### Planned topics:

1. Simplicial and singular homology
2. Basic homological algebra (chains and homotopies, categories and functors)
3. Degree, CW-homology
4. Cohomology, ring structure
5. Orientability, Poincare duality
6. Obstruction theory
7. Fiber bundles, principal bundles
8. Classification of vector bundles
9. Characteristic classes
10. Advanced homological algebra, Hom, Tensor
11. Derived functors

### Prerequisites:

__Basic algebra__: vector spaces, groups, factor groups, homomorphisms, rings and ideals.  
__Basic topology__: topological spaces, continuous maps, homeomorphisms, homotopy, constructions.

[Here](https://webspace.maths.qmul.ac.uk/i.goldsheid/MTH732U/summary1.pdf) is a summary/review of basic topology (from a course at the Queen Mary University, London). 


### Text:

- Allen Hatcher: [Algebraic Topology](https://pi.math.cornell.edu/~hatcher/AT/ATpage.html)
- Joseph Rotman: Introduction to Homological Algebra

## Homework

- [HW 1](HW1.pdf) Due: 25/02
- [HW 2](HW2.pdf) Due: 25/03
- HW 3
- HW 4
- HW 5

## Midterms
1. March 18  
<!--Topics: Commutative algebra, prime and maximal ideals, radicals, affine varieties, Hilbert's Nullstellensatz, Noetherian rings, Hilbert's basis theorem, Noether-Lasker theorem, Noetherian topological spaces, Noether's Normalisation Lemma, localisation, sheaf of regular functions  -->
2. May 6  
<!-- Topics: prevarieties, gluing, varieties, projective varieties, homogeneous coordinates, Segre embedding, Veronese embedding, Grassmannian variety, birational maps, function field, blow-up, tangent cone-->

Format: 30 minutes, 2 questions (1 theory + 1 problem very similar to a homework exercise)
