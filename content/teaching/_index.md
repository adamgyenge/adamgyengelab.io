---
title: Teaching
---

__Office__: Building H, 5th floor, room 505

__Office hours__: Tuesdays 8.30-10. If you want to attend, please send me an email on the previous day or earlier!

## Current teaching

- [Algebraic topology and homological algebra](algtop/2025)
- [Informatics 3](info3/2025)
- [Programming exercises in algorithm theory](progalg/2025en)
- [Algoritmuselmélet programozási feladatok](progalg/2025hu)

## Previous terms

### 2024/2025/1

- [Commutative algebra and algebraic geometry](algeo/2024)
- [Programming exercises in probability theory](progval/2024en)
- [Valószínűségszámítás programozási feladatok](progval/2024hu)
- Vector and matrix algebra (practice session)

### 2023/2024/2

- Programming exercises in algorithm theory
- Algoritmuselmélet programozási feladatok
- Mathematics A2

### 2023/2024/1

- Commutative algebra (Advanced Linear Algebra)
- Mathematics A3
- Probability theory programming exercises
- Valószínűségszámítás programozási feladatok
- Vector and matrix algebra (practice session)

### 2022/23/2

- Lie groups and Lie algebras
- Mathematics A2
- Programming exercises in algorithm theory
- Algoritmuselmélet programozási feladatok

### 2021/22/1

- Algebra 1 (practice session) 

## Old courses

### University of Oxford, Jesus College
- Metric Spaces and Complex Analysis, Linear Algebra (tutorials), 2020/21 Michaelmas Term
- Groups and Group Action, Analysis III (tutorials), 2019/20 Trinity Term
- Analysis II, Linear Algebra II (tutorials), 2019/20 Hilary Term
- Metric Spaces and Complex Analysis, Linear Algebra (tutorials), 2019/20 Michaelmas Term
- Constructive Mathematics (tutorials), 2018/19 Trinity Term

### University of British Columbia
- Math 253 (Section 203), 2018/19 Winter Term 1
- Math 105 (Section 209), 2017/18 Winter Term 2
- Math 104 (Section 107), 2017/18 Winter Term 1
- Math 105 (Section 202), 2016/17 Winter Term 2
- Math 102 (Section 107), 2016/17 Winter Term 1

### Eötvös Loránd University
- Linear algebra, 2014/15/1
- Linear algebra, 2013/14/1

### Budapest University of Technology and Economics
- Mathematics A1, 2015/16/1
- Mathematics A3, 2012/13/1
- Mathematics A3, 2011/12/1
- Introduction to computer science II., 2007/08/1
- Theory of algorithms, 2006/07/2