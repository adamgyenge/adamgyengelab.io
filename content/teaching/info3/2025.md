---
title: Informatics 3
---

# Informatics 3

## Course info

- Class hours: Tuesday (room 405A) & Thursday (room 207) 8.30-10.00 
- Location: BME campus, building H. 
<!-- Office hours: Before or after the Tuesday classes (13.30-14.15 and 16-16.30), or ask in email. -->

Neptun code: BMETE91AM44  
Credits: 4  
Final grade: Project + 2 midterms + participation

## Content

1. Scientific programming in Python
- Advanced features of NumPy and SciPy
- Symbolic computations with SymPy
- An outlook to SAGE
2. Computational topology
- Basics of topology
- Knots and links
- 2-manifolds
- Triangulations and simplicial complexes

## Notes

- [SP] Ádám Gyenge, Ferenc Wettl: [Scientific programming in Python](notes.pdf)  
(Password: the programming language we use with small letters)  
- [EH] Edelbrunner-Harer: [Computational topology](https://www.maths.ed.ac.uk/~v1ranick/papers/edelcomp.pdf)
- [Solution](lab4sol.pdf) to an exercise from Lab 4

## Schedule

| Week | Lecture | Lab | Notes |
|-----|--------------|------------|----|
| 1 | [Python scientific ecosystem, advanced NumPy and SciPy](lec1.pdf) | [Lab 1](/teaching/info3/l1) | [SP] 5-6 |
| 2 | [SymPy: symbols](lec2.pdf) | [Lab 2](/teaching/info3/l2) | [SP] 7.1 |
| 3 | [Calculus, DE](lec3.pdf) | [Lab 3](/teaching/info3/l3) | [SP] 7.2, 7.3 |
| 4 | [Polynomials](lec4.pdf) | [Lab 4](/teaching/info3/l4) | [SP] 7.4 |
| 5 | [Algebra, number theory](lec5.pdf) | [Lab 5](/teaching/info3/l5) | [SP] 7.5, 7.6 |
| 6 | SAGE | Midterm 1, project ideas | [SP] 7.5 |
| 7 | Intro to topology | Lab 7 | [EH] 1.1-1.2 |
| 8 | Knots and links | Lab 8 | [EH] 1.3 |
| 9 | Two-dimensional manifolds | Lab 9 | [EH] 2.1 |
| 10 | Triangulations, simplicial complexes, Euler-char | No lab | [EH] 2.2, 2.4 |
| 11 | Break | No lab | |
| 12 | Midterm 2 | No lab | |
| 13 | Bonus: homotopy, fundamental group | Projects | |
| 14 | No class | No lab | |


## Midterms

Format: 60 minutes, 4 questions 

1st Midterm: NumPy, SciPy, SymPy, symbols, expressions, simplification, expression trees, substitution, lambdification, calculus, differential equations, polynomials, ring and integral domains, linear algebra (numerical and symbolic), ideals, modules, number theory

<!--(2 theory + 2 problem very similar to lab exercises)-->
