## Jordan curve

Recall the Jordan Curve Theorem which says
that every simple closed curve in the plane decomposes R^2
into two.
- Show the same is true for a simple closed curve on the sphere, S^2 ={x ∈ R^2: ||x|| = 1}.
- Give an example that shows the result does not hold for simple closed curves on the torus.

## Homeomorphisms
Give explicit homeomorphisms to show that the following spaces with topologies inherited from the respective
containing Euclidean spaces are homeomorphic:
- R, the real line;
- (0, 1), the open interval;
- S^1 − {(0, 1)}, the circle with one point removed.

## Union-Find
Implement the Union-Find datastructure/algorithm in Python.