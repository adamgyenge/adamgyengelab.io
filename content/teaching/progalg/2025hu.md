---
title: Algortimuselmélet programozási feladatok
---

## Alapvető információk

Neptun kód: BMETE91AM57  
Kredit: 2  
Órarend és helyszín: Kedd, 16.15-17.00, T604

## Részletek

A tárgy célja az Algoritmuselmélet című tárgy tematikájához kapcsolódó programozási feladatok megoldásán keresztül a hallgatók programozási képességeinek szinten tartása, és egyúttal az algoritmusok működésének, hatékonyságának, bonyolultságának... jobb megértése.

Olyan feladatokat igyekszünk adni, melyek érdekesek lesznek mindenki számára, és amelyek megoldása sok haszonnal fog járni. A feladatok száma **10**. Mindegyik határidőre leadott és helyesen megoldott feladat **1** pontot ér. Részmegoldásért vagy későn leadott feladatért nem jár pont. Minimum követelmény a feladatok legalább **40%**-ának (azaz **4** feladatnak) határidőre való leadása. 

A feladatok megoldását az **[codepost.io](https://codepost.io)** rendszerben kell feltölteni. Ide mindenki fog kapni egy meghívót a félév elején a Neptunban megadott email címére, és ezzel a meghívóval kell beregisztrálni. A rendszer automatikusan ellenőrzi a házifeladatokat. Az automatikus tesztek nagyon érzékenyek a generált output formátumára, ezért nagyon fontos hogy mindenki a legpontosabban kövesse az egyes feladatokban megadott specifikációkat, különös tekintettel az output formátumára. A határidőig akárhányszor fel lehet tölteni megoldást, ezek közül mindig az utolsó az érvényes. 

A feltöltéskor egyből látszik hogy a teszt elfogadta-e a megoldást vagy sem ('Grade' mező). A rendszer használatáról [itt találni segítséget](https://help.codepost.io/en/collections/1851564-students-student-console). 

Ha bármilyen kérdés van akár a feladatokkal, akár a CodePost.io-val kapcsolatban, a **{gyenge.adam} (at) ttk [dot] bme [dot] hu** címre lehet írni.   

## Szabályok

- Minden feladatot *Python3*-ban kell megoldani és .py kiterjesztésű fájlként kell feltölteni pontosan a feladatban megadott névvel! Erősen ajánlott a [Visual Studio Code](https://code.visualstudio.com/) használata a programozáshoz ebben a kurzusban. Itt található egy [anyag](install_pvsc.pdf) arról, hogyan állíthatod be Pythonban való kódoláshoz.
- Ha a [Jupyter](https://jupyter.org/)-t is telepíted, a segédanyagokat .ipynb formátumban is olvashatod. Egyébként html-ben olvashatod őket.
- Több feladathoz is biztosítunk csontváz fájlokat. Ha van egy ilyen vázfájl, akkor ebbe írd bele a megoldást. Ha nincs ilyen fájl, használd a következő vázat:
```
def main():
    # Ide írd az algoritmust

if __name__ == "__main__":
    main()
    # Ide ne írj semmit
```

- Függvényeken belül tilos függvényeket definiálni.
- Minden programkód elejére megjegyzésként az alábbi szöveget kell írni:

"""
E programot magam kódoltam, nem másoltam vagy írtam át más kódját, és nem adtam át másnak!
Aláírás saját névvel
""" 

Az, hogy a program kódját mindenkinek magának kell beírnia, programkódot átadnia, mástól kérnie és elfogadnia, letöltenie és átírnia nem szabad, nem zárja ki az együtt gondolkodás vagy a segítségkérés lehetőségét!  

Csak az első óra lesz hagyományos. A többi óra konzultáció jellegű. Ha valaki szeretne ezekre a konzultációkra jönni, kérem emailben szóljon előre, és jöjjön a H505-be.


## Végső jegy

0-39% - 1  
40-49% - 2  
50-59% - 3  
60-69% - 4  
70-100% - 5  
  
## Menetrend

| Hét | Tantermi óra | Segédanyag | HF | Határidő |
|-----|--------------|------------|----|----------|
| 1 | Van |  | [1. HF](01/01hu.html) | 2025.02.25, 22.00 |
| 2 | Nincs |  | [2. HF](02/02hu.html) | 2025.03.05, 22.00 |
| 3 | Nincs |  | [3. HF](03/03hu.html) | 2025.03.12, 22.00 |
| 4 | Nincs |  | [4. HF](04/04hu.html) | 2025.03.19, 22.00 |
| 5 | Nincs | | [5. HF](05/05hu.html) | 2025.03.26, 22.00 |
| 6 | Nincs | | [6. HF](06/06hu.html) | 2025.04.02, 22.00 |
| 7 | Nincs | | [7. HF](07/07hu.html) | 2025.04.09, 22.00 |
| 8 | Nincs | | [8. HF](08/08hu.html) | 2025.04.16, 22.00 |
| 9 | Szünet | | -- | |
| 10 | Nincs | | [9. HF](09/09hu.html) | 2025.04.30, 22.00 |
| 11 | Nincs | | [10. HF](10/10hu.html) | 2025.05.07, 22.00 |
