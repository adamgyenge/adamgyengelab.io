1. Tasks that can be run from the command line should always have a ```main()``` function that should be called as follows:
```
if __name__ == "__main__":
    main()
```
2. For plotting problems, always load the matplotlib library as follows:
```import matplotlib.pyplot as plt```
3. The submitted tasks will be tested in a Python 3.7 environment (for example, the ``math.comb()``` function is not implemented).