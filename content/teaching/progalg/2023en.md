---
title: Programming exercises in algorithm theory 
---

## Basic info

Neptun code: BMETE91AM57  
Credits: 2  
Class hour and location: Thursday 14.15-15.00, H601  

## Details

The aim of this subject is to maintain the programming skills of the students through the solution of the programming tasks related to the Theory of Algorithms, and at the same time to facilitate a better understanding of the algorithms and the efficiency and complexity of them.

We are trying to give you interesting tasks that the solution will be of great benefit for every student. The number of tasks is **10**. Each solved task worth **1** point if it is submitted until the deadline. Partial solutions or late submissions do not get credit. The requirement is to submit at least **40%** of the tasks by deadline (that is, at least **4** tasks). The program code must be submitted in email to **{gyenge.adam} (at) ttk [dot] bme [dot] hu** as an attachment having the extension .py (files with extension .ipynb should not be submitted), and in a separate file the data when the exercise prescribes that. The subject of the email is "ProgAlg - " followed by the number of the exercise (E.g. ProgAlg - 5). The file name of the code starts with the job number, followed by the name of the student without spaces. E.g. Adrian Smith's 5th homework is named 5AdrianSmith.py. Please never use space in filename. The program have to be written in Python 3. The following text must be added as a comment into every program code:

"""
I encoded this program myself, did not copy or rewrite the code of others,
and did not give or send it to anyone else.  

Your Name
"""
You have to follow the next rules. You must not submit or look at solutions or program code that are not your own. It is an act of plagiarism to submit work that is copied or derived from the work of others and submitted as your own. You must not share your solution code with other students, nor ask others to share their solutions with you. You must not ask anybody to write code for you. But many forms of collaboration are acceptable and even encouraged. It is usually appropriate to ask others for hints and debugging help or to talk generally about problem solving strategies and program structure. In fact, we strongly encourage you to seek such assistance when you need it. Discuss ideas together, but do the coding on your own.

The classes will serve as office hours; no new material will be taught.

## Final grade

0-39% - 1  
40-49% - 2  
50-59% - 3  
60-69% - 4  
70-100% - 5  
  
[Points](https://docs.google.com/spreadsheets/d/e/2PACX-1vTihsD4RztwrDn6eH38jIFHzvtrXwcNKQJzjdAJkjHQzOkSVNlOfpj3s0rGKWIexQrYiwJtvxOuSAgI/pubhtml?gid=91470575&single=true)


## Exercises

- [Week 1](01/01en.html) Deadline: 14/03/2023, 24.00
- [Week 2](02/02en.html) Deadline: 21/03/2023, 24.00
- [Week 3](03/03en.html) Deadline: 28/03/2023, 24.00
- [Week 4](04/04en.html) Deadline: 04/04/2023, 24.00
- [Week 5](05/05en.html) Deadline: 18/04/2023, 24.00
- [Week 6](06/06en.html) Deadline: 25/04/2023, 24.00 **Warning: No class on the 13th of April.**
- [Week 7](07/07en.html) Deadline: 02/05/2023, 24.00
- [Week 8](08/08en.html) Deadline: 09/05/2023, 24.00
- [Week 9](09/09en.html) Deadline: 16/05/2023, 24.00
- [Week 10](10/10en.html) Deadline: 23/05/2023, 24.00