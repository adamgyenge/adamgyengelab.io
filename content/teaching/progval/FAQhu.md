1. A parancssorból futtatható feladatoknak mindig legyen egy ```main()``` függvénye ami a következőképpen legyen meghívva:  
```
if __name__ == "__main__":
    main()
```
2. A rajzolási feladatoknál a matplotlib könytárat mindig a következőképpen töltsük be:  
```import matplotlib.pyplot as plt```
3. A beadott feladatokat Python 3.7 környezetben fogjuk tesztelni (így pl a ```math.comb()``` függvény nincs implementálva).