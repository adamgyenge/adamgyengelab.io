---
title: Valószínűségszámítás programozási feladatok
---

Valószínűségszámítás programozási feladatok
===

## Alapvető információk

Neptun kód: BMETE91AM46  
Kredit: 2  
Órarend és helyszín: Csütörtök 9.15-10.00, H507

## Részletek

A tárgy célja a Valószínűségszámítás című tárgy tematikájához kapcsolódó programozási feladatok megoldásán keresztül a hallgatók programozási képességeinek fejlesztése.

Olyan feladatokat igyekszünk adni, melyek érdekesek lesznek mindenki számára, és amelyek megoldása sok haszonnal fog járni. A feladatok száma **10**. Mindegyik határidőre leadott és helyesen megoldott feladat **1** pontot ér. Részmegoldásért vagy későn leadott feladatért nem jár pont. Minimum követelmény a feladatok legalább **40%**-ának (azaz **4** feladatnak) határidőre való leadása. 

A feladatok megoldását az **[codepost.io](https://codepost.io)** rendszerben kell feltölteni. Ide mindenki fog kapni egy meghívót a félév elején a Neptunban megadott email címére, és ezzel a meghívóval kell beregisztrálni. A rendszer automatikusan ellenőrzi a házifeladatokat. Az automatikus tesztek nagyon érzékenyek a generált output formátumára, ezért nagyon fontos hogy mindenki a legpontosabban kövesse az egyes feladatokban megadott specifikációkat, különös tekintettel az output formátumára. A határidőig akárhányszor fel lehet tölteni megoldást, ezek közül mindig az utolsó az érvényes. 

A feltöltéskor egyből látszik hogy a teszt elfogadta-e a megoldást vagy sem ('Grade' mező). A rendszer használatáról [itt találni segítséget](https://help.codepost.io/en/collections/1851564-students-student-console). 

Ha bármilyen kérdés van akár a feladatokkal, akár a CodePost.io-val kapcsolatban, a **{gyenge.adam} (at) ttk [dot] bme [dot] hu** címre lehet írni.   

Kövessük ezeket a [programozási szabályokat](FAQhu) a gyakori hibák elkerülésére.  

Minden feladatot Python3-ban kell megoldani és .py kiterjesztésű fájlként kell feltölteni pontosan a feladatban megadott névvel! Minden programkód elejére megjegyzésként az alábbi szöveget kell írni:

"""
E programot magam kódoltam, nem másoltam vagy írtam át más kódját, és nem adtam át másnak!
Aláírás saját névvel
"""

Az, hogy a program kódját mindenkinek magának kell beírnia, programkódot átadnia, mástól kérnie és elfogadnia, letöltenie és átírnia nem szabad, nem zárja ki az együtt gondolkodás vagy a segítségkérés lehetőségét!  

Az első, második és negyedik órán röviden átnézzük a Python alapjait, a véletlenszám generálást illetve a grafikonok készítését az alábbi segédanyagok alapján. A többi óra konzultáció jellegű, az esetleges segédanyagok önállóan is átnézhetők. Ha valaki szeretne ezekre a konzultációkra jönni, kérem emailben szóljon előre, és jöjjön a H505-be.

## Végső jegy

0-39% - 1  
40-49% - 2  
50-59% - 3  
60-69% - 4  
70-100% - 5  
  
## Menetrend

| Hét | Tantermi óra | Segédanyag | HF | Határidő |
|-----|--------------|------------|----|----------|
| 1 | Van | [Valószínűség Pythonban](01/01hu_deMere.ipynb) | [1. HF](01/01hu.html) | 2023.09.19, 22.00 |
| 2 | Van | [Listák, feltételes valószínűség](02/02hu_MontyHall.ipynb) | [2. HF](02/02hu.html) | 2023.09.26, 22.00 |
| 3 | Nincs |  | [3. HF](03/03hu.html) | 2023.10.03, 22.00 |
| 4 | Van | [Grafikonok](04/04hu_plotting.ipynb) | [4. HF](04/04hu.html) | 2023.10.10, 22.00 |
| 5 | Nincs | | [5. HF](05/05hu.html) | 2023.10.17, 22.00 |
| 6 | Nincs | | [6. HF](06/06hu.html) | 2023.10.24, 22.00 |
| 7 | Nincs | [Scipy, Sympy](07/07hu_integral.html) | [7. HF](07/07hu.html) | 2023.10.31, 22.00 |
| 8 | Nincs | | [8. HF](08/08hu.html) | 2023.11.07, 22.00 |
| 9 | Nincs | [Numpy](09/09numpy.html) | [9. HF](09/09hu.html) | 2023.11.14, 22.00 |
| 10 | Nincs | | [10. HF](10/10hu.html) | 2023.11.21, 22.00 |