---
title: VIK Matematika A2
---

## Információk

Neptun kód: BMETE90AX59  
Órarend, helyszín:  
- V10i kurzus: Szerda, 10:15-12.00, IB138  
- V4 kurzus: Szerda, 12:15-14.00, E407 
 
Az előadás honlapja

## Gyakorlat anyagok

- [Gyak 1-2](gyak1-2.pdf)
- [Gyak 3](gyak3.pdf)
- [Gyak 4-5](gyak4-5.pdf)
- [Gyak 6-7](sheet6-7.pdf)