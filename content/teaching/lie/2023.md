---
title: Lie Groups and Lie Algebras
---

## Course info

Neptun code: BMETE91MM22  
Credits: 6  
Class hours (tentative):  
- Tuesday 16.00-17.30, H405A
- Wednesday 16.00-17.30, H405A

## Announcements

No class on April 5.

## Final grade

Homeworks + presentation

## Documents

- [Course description](description.pdf)
- [Lecture plan](plan.pdf)
- [Notes for the talk of Gábor Takács](PLnotes.pdf)


## Homework

- [HW 1](HW1.pdf) Due: 21/03
- [HW 2](HW2.pdf) Due: 18/04
- [HW 3](HW3.pdf) Due: 09/05
- [HW 4](HW4.pdf) Due: 24/05