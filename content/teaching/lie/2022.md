---
title: Lie Groups and Lie Algebras
---

## Course info

Neptun code: BMETE91MM22  
Credits: 6  
Class hours:  
- Monday 16.00-17.30
- Wednesday 12.00-13.30

## Office hours

Ask in email, but preferably, Monday, 15.45.

## Final grade

Homeworks + presentation

## Documents

- [Course description](description.pdf)
- [Lecture plan](plan.pdf)


## Problem sheets

- [Sheet 1](sheet1.pdf)
- [Sheet 2](sheet2.pdf)
- [Sheet 3](sheet3.pdf)
- [Sheet 4](sheet4.pdf)
- [Sheet 5](sheet5.pdf)
- [Sheet 6](sheet6.pdf)
- [Sheet 7](sheet7.pdf)
- [Sheet 8](sheet8.pdf) 
- [Sheet 9](sheet9.pdf)
- [Sheet 10](sheet10.pdf)
- [Sheet 11](sheet11.pdf)