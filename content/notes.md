---
title: Notes & Events
---

## Slides & Lecture Notes

1. [Hilbert scheme of points on simple singularities](singhilbpres_mainz.pdf) (07/01/2016, Mainz)
2. [Cayley octonions and the $G_2$ Lie group](cayleyg2.pdf) (15/10/2015, Szeged)
3. [Introduction to étale cohomology](etalecohom.pdf) (16/06/2014, Budapest)
4. [The Lorentz attractor](lorentz.pdf) (in Hungarian)
5. [Statistical methods for the investigation of scale-free networks](tdk_ea.pdf) (in Hungarian)  (17/11/2007, Budapest)

## Events I (co)organize(d)

### Conferences, Workshops

1. [Némethi 60: Geometry and Topology of Singularities](https://www.renyi.hu/conferences/nemethi60/) (Budapest, May 27-31, 2019)
2. [Summer school on the applications of étale cohomology](ss2014) (Budapest, 2014)

### Seminars
1. [BME Algebra and Geometry Seminar](https://geometria.math.bme.hu/tanszeki-szeminariumok) (2023-)
2. [UBC Algebra and Algebraic Geometry Seminar](https://wiki.math.ubc.ca/mathbook/alggeom-seminar/Main_Page) (2016-2018)
3. [Vector Bundles Learning Seminar](seminar/2016s) (2016)
4. [Conformal Field Theory Learning Seminar](seminar/2014f) (2014-15)

## Press Articles

1. [Mathematical Village in Turkey](matfalu.pdf) (in Hungarian), Természet Világa, July 2016.