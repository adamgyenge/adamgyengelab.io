---
title: Home
---


# Ádám Gyenge



<center>
<div class="image">
<img style="border: 1px solid #648880;
  padding: 4px 4px 4px 4px;
   width: 210px;" alt="Me" src="photo.jpg">
</div> 
</center>

<!--
<div style="margin-top:0;margin-bottom:0px;font-size:24px;font-weight:600">Short Bio
<hr style="height:1px;margin-top:3px;margin-bottom:0px">
</div>
-->

<!--## Basic info-->

Associate professor  
[Department of Algebra and Geometry, Institute of Mathematics](https://math.bme.hu/)  
[Budapest University of Technology and Economics](https://www.bme.hu/)

__E-mail__: **{gyenge.adam} (at) ttk [dot] bme [dot] hu**

__Office__: H.505

### Research interests

Algebraic geometry, representation theory   
Particularly: Moduli spaces of sheaves and curves, derived categories, singularity theory, curve counting invariants, vertex algebras, quantum connection

<!--
### Brief bio

I completed my PhD in 2016 at the [Eötvös Loránd University](https://www.elte.hu/) in Budapest, Hungary. From 2016 until 2019 I was a Postdoctoral Researcher at the [University of British Columbia](https://www.math.ubc.hu/), Canada. From 2019 until 2021 I was a Postdoctoral Research Assistant at the [University of Oxford](https://www.maths.ox.ac.uk/), UK. In 2021--2022 I was a Marie Skłodowska-Curie Fellow at the Alfréd Rényi Institute of Mathematics, Hungary.
-->
Here is a [link to my full CV](https://gitlab.com/adamgyenge/cv/-/raw/master/cv.pdf).

## Students

[For potential students](potential)

### PhD

Lukas Bertsch (University of Vienna, joint supervision with Balázs Szendrői), 2022-

## Contact

Tel: + 36 1 463-1111/5678

### Address

Budapest University of Technology and Economics  
Institute of Mathematics  
Egry József utca 1  
H-1111, Budapest  
Hungary
