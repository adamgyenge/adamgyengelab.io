+++
headless = true
+++

- [Home]({{< relref "/" >}})
- [CV](https://gitlab.com/adamgyenge/cv/-/raw/master/cv.pdf)
- [Publications]({{< relref "/docs/publications" >}})
- [Notes & Events]({{< relref "/docs/notes" >}})
- [Teaching]({{< relref "/docs/teaching" >}})