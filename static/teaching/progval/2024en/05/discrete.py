from scipy.special import comb


class Drv(object):
    """
    Define a class for discrete random variable

    xk is a list of monoton increasing values
    pk is the list of probabilities belonging to xk
    """
    
    def __init__(self, xk=[0], pk=[1]):
        self.xk = xk
        self.pk = pk

    def e(self):
        """
        Return the expected value of the discrete random variable.
        """
        return # return here the value

    def is_nonneg(self):
        """
        Return True if the random variable is non negative.
        Otherwise False.
        """
        return # return here True or False

    def reweight(self):
        """
        Reweighting a random variable using the expected 
        value of the random variable. Return the new variable.
        """
        #pknew = [1]
        # write the code here
        

        return Drv(self.xk,pknew)


class Binomial(Drv):
    """
    Class for binomial random variable derives from Drv.
    Parameters needed: n, p.
    """
    
    def __init__(self, n=1, p=1):
        self.n = n
        self.p = p
        # define the values and probabilities of the binomial variable here
        super().__init__(values, probabilities) # inheritance

    def e(self):
        """
        Return the expected value of the binomial random variable.
        """
        return # return the value here


class Uniform(Drv):
    """
    Class for a uniform random variable derives from Drv.
    n:   the number a values (which are 1,2,...,n)
    """

    def __init__(self, n=1):
        self.n = n
        # define the values and probabilities of the uniform variable here
        # and complete the code

    def e(self):
        """
        Return the expected value of the uniform random variable.
        """
        return # return the value here
