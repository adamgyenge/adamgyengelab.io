import math
import matplotlib.pyplot as plt
import numpy as np



def log_table_eval(n):
    """
    Evaluate log(a) + log(b) where a and b run from 1 to n

    :param n: the upper limit for a and b
    :return:  an (n + 1) x (n + 1) matrix with the function values
    """

    log_table = np.zeros((n + 1, n + 1), float)
    for a in range(1, 1 + n):
        for b in range(1, 1 + n):
            log_table[a, b] = math.log(a,2) + math.log(b,2)
    return log_table

def log_table_draw(n):
    """
    Draw the values log(a) + log(b) where a and b run from 1 to n

    :param n:  the upper limit for a and b
    """
    log_table = log_table_eval(n)
    plt.title("log(a) + log(b)")
    plt.imshow(log_table, origin='lower')
    plt.colorbar()

def main():
    N = 100
    fig = plt.figure(figsize=(12,4))

    plt.subplot(131)
    log_table_draw(N)
    
    # TODO: draw the remaining plots in positions 132 and 133
    
    plt.tight_layout()
    plt.show()
    return fig

if __name__ == '__main__':
    main()
