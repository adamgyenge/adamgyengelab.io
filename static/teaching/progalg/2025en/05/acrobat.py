
class Acrobat:
    """
    A class containing the data of an acrobat
    """

    def __init__(self, identifier, height, weight):
        """
        Initialize a new Acrobat object.

        :param identifier: identification number of acrobat
        :param height: height of acrobat
        :param weight: weight of acrobat
        """
        self.id = identifier
        self.height = height
        self.weight = weight

    def may_stand_on(self, other):
        """
        Desides if the acrobat may stand on the other acrobat.

        :param other: another acrobat the actual acrobat wants to stand on.
        :returns: True, if the acrobat may stand on the other, otherwise False.
        """
        return self.weight < other.weight and self.height < other.height

    def __str__(self):
        """
        Convert the acrobat into string

        :returns: the string representation of the acrobat
        """
        return f'Acrobat{self.id}: height = {self.height}, weight = {self.weight}'

def read_acrobats(f):
    """
    Read the acrobats from a file given in the command line.

    :param f: the file object to be read in
    :returns: list of the acrobats
    """
    acrobats = []
    i = 0
    for line in f:
        i += 1
        h, w = [int(a) for a in line.split()]
        acrobats.append(Acrobat(i, h, w))
    return acrobats

if __name__ == '__main__':
    import sys
    with open(sys.argv[1], 'r') as f:
        acrobats = read_acrobats(f)

    acrobats.sort(key=lambda x: x.height, reverse=True)

