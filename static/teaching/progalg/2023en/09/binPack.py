import random
import math

def first_fit(lst):
    """
    Place the packs into bins by the first-fit algorithm.

    The algorithm try to place the packs (keeping their order) into
    the bins so that the next item is always placed in the first bin
    where it fits. If the pack does not fit in any of the bins, open 
    a new bin. (Every pack is measured by a number between 0 and 1,
    let us call the weight of it. Packs fit into a bin, if the sum 
    of their weights is not more than 1.)

    :param lst: List of the packs.
    :return:    The number of necessary bins.
    """
    # TODO Write here the code of FF algorithm!
    return -1

def first_fit_d(lst):
    """
    Place the packs into bins by the first-fit-decreasing algorithm.

    The algorithm try to place the packs into the bins like in the
    previous algorithm but first sort them decreasing by weights.

    :param lst: List of the packs.
    :return:    The number of necessary bins.
    """
    # TODO Write here the code of FFD algorithm!
    return -1

def best_fit(lst):
    """
    Place the packs into bins by the best-fit algorithm.

    The algorithm try to place the packs (keeping their order) into
    the bins so that the next item is always placed in the bin
    where the omittance is minimal.

    :param lst: List of the packs.
    :return:    The number of necessary bins.
    """
    bins = []
    for pack in lst:
        max_fulness = -1.0
        max_pos = -1
        for i in range(len(bins)):
            if (bins[i] + pack <= 1.0) and (bins[i] > max_fulness):
                max_pos = i
                max_fulness = bins[i]
        if max_pos == -1:
            bins.append(pack)
        else:
            bins[max_pos] += pack
    return len(bins)

if __name__ == '__main__':
    # TODO Generate a random sequence of 1000 numbers between 0 and 1
    # instead of the next list:
    packs = [0.34, 0.34, 0.33, 0.33, 0.33, 0.33]

    opt = math.ceil(sum(packs)) 
    ff = first_fit(packs)
    ffd = first_fit_d(packs)
    bf = best_fit(packs)

    print(f"OPT: {opt}")
    print(f" FF: {ff}")
    print(f"FFD: {ffd}")
    print(f" BF: {bf}")
