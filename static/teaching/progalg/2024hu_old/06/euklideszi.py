import math
import matplotlib.pyplot as plt
import numpy as np

N = 100

def log_tabla_kiszamol(n):
    """
    Kiszámolja a log(a) + log(b) értéket az 1-től n-ig terjedő
    számpárokra

    :param n: Meddig számoljuk a táblázat értékeit
    :return: Egy (n + 1) x (n + 1)-es numpy mátrix feltöltve
             a függvényértékekkel
    """

    log_tabla = np.zeros((n + 1, n + 1), np.float)
    for a in range(1, 1 + n):
        for b in range(1, 1 + n):
            log_tabla[a, b] = math.log(a,2) + math.log(b,2)
    return log_tabla

def log_tabla_rajzol(n):
    """
    Kirajzolja a log(a) + log(b) értéket az 1-től n-ig terjedő
    számpárokra

    :param n: Meddig ábrázoljuk a függvényértékeket
    """
    log_tabla = log_tabla_kiszamol(n)
    plt.title("log(a) + log(b)")
    plt.imshow(log_tabla, origin='lower')
    plt.colorbar()

if __name__ == '__main__':
    plt.figure(figsize=(5,4))

    plt.subplot(111)
    log_tabla_rajzol(N)

    plt.tight_layout()
    plt.show()
