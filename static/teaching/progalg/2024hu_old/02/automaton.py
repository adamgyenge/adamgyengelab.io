#!/usr/bin/env python3

# START

# Kezdőállapot
q0 = 0

# Elfogadó állapotok
f = {0, 1, 3}

def delta(q, a):
    if q == 0:
        if a == 'a':
            return {1, 2}
        else:
            return set()
    elif q == 1:
        if a == 'a':
            return {1}
        else:
            return set()
    elif q == 2:
        if a == 'b':
            return {3}
        else:
            return set()
    elif q == 3:
        if a == 'a':
            return {2}
        elif a == 'b':
            return set()
        else:
            return {3}
    else:
        return set()

# END

def felismer(szo):
    qs = set([q0])
    for a in szo:
        qs_vesszo = set()
        for q in qs:
            qs_vesszo.update(delta(q, a))
        qs = qs_vesszo
    return f.intersection(qs)

def main(args):
    szo = args[1]
    if felismer(szo):
        print('Elfogadjuk a szot')
    else:
        print('Nem fogadjuk el a szot')
    return 0

if __name__ == '__main__':
    import sys
    sys.exit(main(sys.argv))

