
class Akrobata:
    """
    Akrobaták adatait tároló osztály.
    """

    def __init__(self, azonosito, magassag, suly):
        """
        Létrehoz egy új Akrobata objektumot.

        :param id: Az akrobata sorszáma
        :param magassag: Az akrobata magassága
        :param suly: Az akrobata súlya
        """
        self.id = azonosito
        self.magassag = magassag
        self.suly = suly

    def raallhat(self, masik):
        """
        Eldönti, hogy az adott akrobata ráállhat-e egy másikra

        :param masik: A másik akrobata, akire az aktuális akrobata rá akar állni. 
        :returns: True, ha ráállhat, egyébként False.
        """
        return self.suly < masik.suly and self.magassag < masik.magassag

    def __str__(self):
        """
        Sztringgé alakítja az akrobatát.

        :returns: Az akrobata sztring-reprezentációja.
        """
        return f'Akrobata{self.id}: magasság = {self.magassag}, súly = {self.suly}'

def akrobatakat_olvas():
    """
    Beolvassa az akrobatákat egy parancssorban megadott fájlból.

    :returns: Az akrobatákat tartalmazó lista.
    """
    akrobatak = []
    i = 0
    with open(sys.argv[1], 'r') as f:
        for line in f:
            i += 1
            m, s = [int(a) for a in line.split()]
            akrobatak.append(Akrobata(i, m, s))
    return akrobatak

if __name__ == '__main__':
    import sys
    akrobatak = akrobatakat_olvas()

    akrobatak.sort(key=lambda x: x.magassag, reverse=True)
    # A következő sorok törlendők a végső változatból
    for akrobata in akrobatak:
        print(akrobata)
