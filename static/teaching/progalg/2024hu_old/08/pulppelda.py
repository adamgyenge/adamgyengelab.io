import pulp

def pulp_pelda():
    """
    A standard kimenetre írja a
    max x0 + x1,
    x0 >= 0,
    x1 >= 0,
    2 * x0 + x1 <= 10
    x0 + 2 * x1 <= 10
    égészértékű programozási feladat megoldását.
    """

    # Változók felvétele
    x = [
        pulp.LpVariable("x0", lowBound=0, cat=pulp.LpInteger),
        pulp.LpVariable("x1", lowBound=0, cat=pulp.LpInteger)
    ]
    # Probléma létrehozása
    prob = pulp.LpProblem("Számösszeg", pulp.LpMaximize)
    # Célfüggvény megadása
    prob += pulp.lpSum(x)
    # Korlátok megadása
    prob += 2 * x[0] + x[1] <= 10
    prob += x[0] + 2 * x[1] <= 10

    # Megoldás kiírása
    prob.solve(pulp.apis.coin_api.PULP_CBC_CMD(msg=0))
    # A solver nevének kiírása, hogy a fenti parancsban a msg=0 átadható legyen 
    #print(prob.solver)
    
    print("Maximum értéke:", pulp.value(prob.objective))
    print("Maximum helye:")
    for variable in x:
        print(variable.name + " = " + str(variable.varValue))

if __name__ == '__main__':
    pulp_pelda()
