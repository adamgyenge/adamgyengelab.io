#!/usr/bin/env python
    

def merge(array, l, m, r):
    """
    Merges two subarrays of array[].
    First subarray is array[l..m]
    Second subarray is array[m+1..r]
    """

    # TODO

 
 
def merge_sort(array, l, r):
    """
    MergeSort algorithm

    l is for left index r is right index of the sub-arrays of array[] be sorted
    """
    if l < r:
 
        m = (l+r)//2
 
        # Sort first and second halves
        merge_sort(array, l, m)
        merge_sort(array, m+1, r)

        # Then merge the two parts
        merge(array, l, m, r)

def main(argv):
    with open(argv[1], 'r') as f:
        string_array = f.read().splitlines()
        int_array = [int(i) for i in string_array]
        merge_sort(int_array,0,len(int_array)-1)
        print(int_array)

if __name__ == '__main__':
    import sys
    main(sys.argv)
