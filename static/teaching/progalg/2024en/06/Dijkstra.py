import numpy as np

class Graph():
 
    def __init__(self, vertices):
        self.V = vertices
        self.graph = np.array([[0 for column in range(vertices)]
                      for row in range(vertices)])
 
    def dijkstra(self, source):
        """
        Function that implements Dijkstra's single source
        shortest path algorithm for a graph represented
        using adjacency matrix representation
        """
        # TODO
        pass
 
if __name__ == '__main__':
    import sys

    g = Graph(9)
    g.graph = np.array([[0, 2, 0, 0, 0, 0, 8, 0, 0],
           [2, 0, 8, 0, 0, 4, 0, 11, 0],
           [0, 8, 0, 7, 0, 0, 0, 0, 2],
           [0, 0, 7, 0, 5, 14, 0, 0, 0],
           [0, 0, 0, 5, 0, 10, 0, 0, 0],
           [0, 4, 0, 14, 10, 0, 2, 0, 0],
           [8, 0, 0, 0, 0, 2, 0, 10, 6],
           [0, 11, 0, 0, 0, 0, 10, 0, 3],
           [0, 0, 2, 0, 0, 0, 6, 3, 0]
           ])
 
    dist = g.dijkstra(int(sys.argv[1]))
    
    print("Vertex \t Distance from Source")
    for node in range(len(dist)):
        print(node, "\t\t", dist[node])