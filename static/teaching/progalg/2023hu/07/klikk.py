import itertools

def grafot_olvas(f):
    """
    Beolvas egy irányítatlan gráfot a paraméterül kapott fájl 
    objektumból

    A fájl első sora n m k alakú 
    A gráf csúcshalmaza V = {0, 1, ..., n - 1}.
    A következő m sor U_i V_i alakú, ami azt fejezi ki, 
    hogy a gráf tartalmazza az U_i V_i irányítatlan élt. 
    A k a keresett klikk elemszáma.

    A függvény visszatérési értéke egy hármas, ami tartalmazza a gráf
    éllistáját, a gráf csucsainak számát és a keresett klikk méretét.
    Az éllista egy szótár, ami az egyes csúcsokhoz a szomszédos 
    csúcsok listáját rendeli. 
    Mivel a gráf irányítatlan, ha az U csúcs listájában szerepel a V,
    akkor a V listájában szerepelni fog az U.

    :param f: A gráfot tartalmazó fájl io objektuma.
    :returns: A gráf éllista szótára, valamint 
              az n és k értéke.
    """
    fejlec = f.readline().split()
    n, m, k = [int(s) for s in fejlec]

    graf = dict((i, []) for i in range(n))
    for j in range(m):
        el = f.readline().split()
        u, v = [int(s) for s in el]
        graf[u].append(v)
        graf[v].append(u)
    return graf, n, k

if __name__ == '__main__':
    import sys
    with open(sys.argv[1], 'r') as f:
        graf, csucsszam, k = grafot_olvas(f)

    # TODO Valósítsuk meg a klikk keresést.
