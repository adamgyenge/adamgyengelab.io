class FaCsucs:
    """
    Bináris keresőfa csúcsát reprezentáló osztály

    A felhasználó közvetlenül nem ezt azt osztályt használja,
    hanem a teljes fát reprezentáló BinarisFa osztályt,
    amely képes meghívni a fa gyökércsúcsának metódusait
    """

    def __init__(self, ertek):
        """
        Létrehoz egy új bináris keresőfa csúcsot
        A csúcs bal és jobb részfája kezdetben üres

        :param ertek: A csúcsban tárolt érték
        """
        self.ertek = ertek
        self.bal = None
        self.jobb = None

    def csucsot_keres(self, ertek):
        """
        Megkeresi a paraméterül kapott értekhez tartozó csúcsot
        Ha az érték nincs a fában, akkor azt a csúcsot adja vissza,
        amelyiknek a gyereke kellene legyen az értéket tartalmazó csúcs

        :param ertek: A keresett érték
        :returns: Az érték keresése során érintett utolsó csúcs
        """
        if ertek < self.ertek and self.bal is not None:
            return self.bal.csucsot_keres(ertek)
        elif ertek > self.ertek and self.jobb is not None:
            return self.jobb.csucsot_keres(ertek)
        else:
            return self

    def szintszam(self):
        """
        Meghatározza a fa szintjeinek számát

        :returns: A szintek száma
        """
        # TODO Határozzuk meg rekurzió segítségével a fa szintjeinek számát!
        return -1

    def preorder_nyomtat(self):
        """
        Kinyomtatja a fa csucsait a preorder bejárás sorrendjében
        """
        # TODO Nyomtassuk ki rekurzió segítségével a fában tarolt értékeket!
        pass

class BinarisFa:
    """
    Bináris fát reprezentáló osztály

    A nemüres fákat egy FaCsucs objektum reprezentálja, a fa gyökere
    """

    def __init__(self):
        """
        Létrehoz egy üres bináris fát
        """
        self.gyoker = None

    def beszur(self, ertek):
        """
        Beszúrja a paraméterül kapott értéket a fába, ha az meg nincs benne.

        :param ertek: A beszúrandó érték
        """
        # TODO Szúrjuk be az értéket a fában a rendezés szerint
        # megfelelő helyre!
        # Ha szükséges, hozzuk létre a fa gyökerét
        # Ha a fának mar van gyökere, a csúcsot_keres metódussal
        # meghatározhatjuk, hogy hova kell kerüljön az érték
        pass

    def keres(self, ertek):
        """
        Ellenőrzi, hogy a paraméterül kapott érték benne van-e a fában

        :param ertek: A keresett érték
        :returns: True, ha az érték benne van a fában, egyébként False.
        """
        if self.gyoker is None:
            return False
        else:
            csucs = self.gyoker.csucsot_keres(ertek)
            return ertek == csucs.ertek

    def szintszam(self):
        """
        Megszámolja a fa szintjeit.

        :returns: A fa szintjeinek a száma.
        """
        if self.gyoker is None:
            return 0
        else:
            return self.gyoker.szintszam()

    def preorder_nyomtat(self):
        """
        Preorder sorrendben kinyomtatja a fában tarolt értékeket úgy,
        hogy minden értéket külön sorba ír ki.
        """
        if self.gyoker is not None:
            self.gyoker.preorder_nyomtat()

if __name__ == '__main__':
    import sys
    with open(sys.argv[1], 'r') as f:
        csucsok = [int(i) for i in f.readline().split()]
    binfa = BinarisFa()
    for c in csucsok:
        binfa.beszur(c)
    binfa.preorder_nyomtat()
    print(f"A fa szintjeinek száma {str(binfa.szintszam())}")
