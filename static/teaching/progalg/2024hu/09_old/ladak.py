import random
import math

def first_fit(lst):
    """
    Ládákba pakolja a tárgyakat a first fit algoritmus szerint.

    A tárgyakat a megadott sorrendben próbálja elhelyezni a ládákba úgy,
    hogy a soron következő tárgy mindig a legelső olyan ládába kerüljön,
    ahova befér. Ha egyetlen ládaba sem fér el a tárgy, akkor új ládát nyit.

    :param lst: Az elpakolandó tárgyak listája.
    :return:    A tárgyak elpakolásához szükséges ládák száma.
    """
    # TODO Valósítsuk meg az FF algoritmust!
    return -1

def first_fit_d(lst):
    """
    Ládákba pakolja a tárgyakat a first fit decreasing algoritmus szerint.

    A tárgyakat súly szerint csökkenő sorrendben próbálja elhelyezni a
    ládákba úgy, hogy a soron következő tárgy mindig a legelső olyan
    ládába kerüljön, ahova befér. Ha egyetlen ládába sem fér el a
    tárgy, akkor új ládát nyit.
    
    :param lst: Az elpakolandó tárgyak listája.
    :return:    A tárgyak elpakolásához szükséges ládák száma.

    """
    # TODO Valósítsuk meg az FFD algoritmust!
    return -1

def best_fit(lst):
    """
    Elpakolja a ládákat a best fit algoritmus szerint.

    A tárgyakat a megadott sorrendben próbálja elhelyezni a ládákba úgy,
    hogy a soron következő tárgy mindig olyan ládába kerüljön, hogy a kimaradó
    hely az adott ládában minimális legyen. Ha egyetlen ládába sem fér el
    a tárgy, akkor új ládát nyit.

    :param lst: Az elpakolandó tárgyak listája.
    :return:    A tárgyak elpakolásához szükséges ládák száma.
    """
    ladak = []
    for targy in lst:
        max_telitettseg = -1.0
        max_melyik = -1
        for i in range(len(ladak)):
            if (ladak[i] + targy <= 1.0) and (ladak[i] > max_telitettseg):
                max_melyik = i
                max_telitettseg = ladak[i]
        if max_melyik == -1:
            ladak.append(targy)
        else:
            ladak[max_melyik] += targy
    return len(ladak)

if __name__ == '__main__':
    # TODO Generáljunk egy 1000 elemű véletlen sorozatot
    # 0 és 1 közötti elemekkel az alábbi helyett:
    targyak = [0.34, 0.34, 0.33, 0.33, 0.33, 0.33]

    opt = math.ceil(sum(targyak))
    ff = first_fit(targyak)
    ffd = first_fit_d(targyak)
    bf = best_fit(targyak)

    print(f"OPT: {opt}")
    print(f" FF: {ff}")
    print(f"FFD: {ffd}")
    print(f" BF: {bf}")
